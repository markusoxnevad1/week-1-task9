﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Creates a collection of animals, and also showcase the overload constructor
            List<Animal> animals = new List<Animal>();
            Animal animal1 = new Animal("Lion    ", "Mammals");
            Animal animal2 = new Animal("Elephant");
            Animal animal3 = new Animal("Anaconda", "Reptile");

            //  Adds animals to the collection
            animals.Add(animal1);
            animals.Add(animal2);
            animals.Add(animal3);

            Console.WriteLine("Welcome to Animal Farm");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Below is a list of the animals you can find within the farm");
            Console.WriteLine("");
            Console.WriteLine("");

            //  Writes out the both the name and group of the animals
            foreach (Animal animal in animals)
            {
                Console.WriteLine("Name: " + animal.name + "   Group: " + animal.group);
            }
        }
    }
}
