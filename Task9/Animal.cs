﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class Animal
    {
        public string name { get; set; }
        public string group { get; set; }
        public Animal(string name)
        {
            this.name = name;
        }
        //  Overload constructor
        public Animal(string name, string group)
        {
            this.name = name;
            this.group = group;
        }
        public string Name()
        {
            return name;
        }
        public string Group()
        {
            return group;
        }
    }
    
}
